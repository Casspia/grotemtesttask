import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SelectItem } from 'primeng/primeng';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  allItems: ItemToShow[];
  groups: SelectItem[];
  selectedItems: any;

  constructor(private http: HttpClient) { }

  async ngOnInit() {
    this.allItems = [];
    this.groups = [];

    this.http.get('https://ssdev.superagent.ru/TestApp/Values/GetWithParent').subscribe(data => {
      this.mapToItems(data);
    });
  }

  mapToItems(data) {
    let idCount = 0;
    data.forEach(item => {
      const details = item.skus;
      const label = item.group ? item.group.name : 'Все';
      const value = item.group ? item.group.name : '';
      this.groups.push({ label: label, value: value });

      details.forEach(d => {
        const itemToAdd = {
          id: idCount++,
          group: item.group ? item.group.name : '',
          name: d ? d.name : '',
          price: d ? d.price : ''
        };
        if (itemToAdd.price) {
          this.allItems.push(itemToAdd);
        }
      });
    });
  }

  addToBasket() {
    if (this.selectedItems) {
      this.selectedItems.forEach(obj => {
        this.allItems = this.allItems.filter(item => item.id !== obj.id);
      });
      this.selectedItems = null;
    }
  }
}

export interface Item {
  skus: [{
    id: number,
    name: string,
    price: string
  }];
  group: {
    id: number,
    name: string
  };
}

export interface ItemToShow {
  name: string;
  price: string;
  id: number;
}
